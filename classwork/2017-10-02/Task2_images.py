import os
from PIL import Image

print('%s >>' % os.path.abspath(os.curdir), end=' ')
input_data = input()
while input_data != 'exit':
    splitted_data = input_data.split()
    if splitted_data[0] == 'ls' or splitted_data[0] == 'dir':
        for i in os.listdir('.'):
            print(i)
    elif splitted_data[0] == 'cat':
        try:
            with open(os.path.join(os.curdir, splitted_data[1])) as file:
                for i in file:
                    print(i, end='')
            print()
        except:
            print('No such file')
    elif splitted_data[0] == 'cd':
        if len(splitted_data) == 1:
            print('Current dir is %s' % os.path.abspath(os.curdir))
        elif splitted_data[1] == '..':
            os.chdir(os.pardir)
            print('Changed dir to %s' % os.path.abspath(os.curdir))
        else:
            try:
                os.chdir(os.path.join(os.curdir, splitted_data[1]))
                print('Changed dir to %s' % os.path.abspath(os.curdir))
            except:
                print('No such directory')
    elif splitted_data[0] == 'mkdir':
        try:
            os.mkdir(splitted_data[1])
        except:
            print('Can not create new folder')
    elif splitted_data[0] == 'image':
        try:
            if splitted_data[1] == '-scale':
                scale_koef = int(splitted_data[2]) / 100
                im = Image.open(splitted_data[3])
                out = im.resize([int(scale_koef * s) for s in im.size])
                out.save(splitted_data[3])
                print('Resized from %s to %s' % (im.size, out.size))
        except:
            print('I do not know what you want to do with this image')
    else:
        print('I do not know your command. :(')
    print('%s >>' % os.path.abspath(os.curdir), end=' ')
    input_data = input()

print('Finished work with Rus_file')
