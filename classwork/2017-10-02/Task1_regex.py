import re

text = 'Some text here 00:00 and there 01:30. It is not time to sleep yet.'

res_list = re.findall(r'(\d{2}):(\d{2})', text)  # Получение всего разом
print(res_list)

average = 0
count = 0
for i in re.finditer(r'(?P<hour>\d{2}):(?P<minute>\d{2})', text):
    print('%s:%s' % (i.group('hour'), i.group('minute')))
    average += int(i.group('hour')) * 60 + int(i.group('minute'))
    count += 1

average /= count
print("Average time is {:0>2}:{:0>2}".format(int(average // 60), int(average % 60)))
