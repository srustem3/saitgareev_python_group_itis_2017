#  encoding: utf-8


def tell_the_date(day, month, year):
    print("Today is %s of month %s of year %s." % (day, month, year))


def tell_the_date2(day=1, month=3, year=2):
    print("Today is {day} of month {month} of year {year}.".format(day=day, month=month, year=year))


tell_the_date(1, 2, 3)
tell_the_date2(1, 2, 3)
#  Значение по умолчанию
tell_the_date2()
tell_the_date2(month=10)


def f4(**kwargs):
    #  kwargs.iteritems()
    for key, value in kwargs.items():
        print(key + value)


f4(student='Rustem', course='Inf')

