from Example2_1 import Car, TeslaCar

a = Car('yellow', 'lada', '1920')
print(a)
print(dir(a))
print(help("".replace))
print(Car)
print(globals())
print(a.__dict__)
print('=====================')
b = TeslaCar('Red', 'Tesla', '2017')
print(b.electricity)
print(b.n)
print(hasattr(b, 'color'))
