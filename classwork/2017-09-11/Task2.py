def format(s, **kwargs):
    for key, value in kwargs.items():
        to_replace = '{%s}' % key
        s = s.replace(to_replace, value)
    return s


print(format("Test {one} test {two}", one='myone', two='mytwo'))
