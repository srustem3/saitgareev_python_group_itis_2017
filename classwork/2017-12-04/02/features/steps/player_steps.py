from behave import given, then
from Player import Player


@given('new player')
def step_impl(context):
    context.p = Player()


@then('it should have name')
def step_impl(context):
    assert context.p.hasattr('name')
