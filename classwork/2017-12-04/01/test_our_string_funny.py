import unittest

from nose_parameterized import parameterized
from our_string import is_prefix

params = [('Nik', 'Nikita'), ('Rus', 'Rustem'), ('Danil', 'Daniil')]


class TestOurString(unittest.TestCase):
    pass


def test_is_prefix(s1, s2):
    def test(self):
        self.assertTrue(is_prefix(s1, s2))


test_case = TestOurString()
for i, pars in enumerate(params):
    test_name = 'test' + str(i)
    setattr(test_case, test_name, test_is_prefix(pars[0], pars[1]))
    unittest.main()
