import unittest

from nose_parameterized import parameterized
from our_string import is_prefix


class TestOurString(unittest.TestCase):
    @parameterized.expand([
        ["Nik", "Nikita"],
        ['Rus', 'Rustem'],
    ])
    def test_is_prefix(self, s1, s2):
        self.assertTrue(is_prefix(s1, s2))


if __name__ == '__main__':
    unittest.main()

#  Запуск из консоли - python -m unittest test_our_string
