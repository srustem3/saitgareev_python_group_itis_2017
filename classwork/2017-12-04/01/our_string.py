def is_prefix(s1, s2):
    """
    :param s1: possible prefix
    :param s2: source string
    :return: true if s1 is prefix of s2, false otherwise
    """
    if len(s1) > len(s2):
        return False
    for (i, j) in zip(s1, s2):
        if i != j:
            return False
    return True
