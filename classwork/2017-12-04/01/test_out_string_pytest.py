import pytest
import behave


@pytest.fixture(scope='function')
def obj():
    class Password:
        def __init__(self):
            self.str = 'parol'
    return Password()


def test_1():
    assert obj.str == 'parol'
    obj.str += '__salt'


def test_2(obj):
    assert obj.str == 'parol__salt'
