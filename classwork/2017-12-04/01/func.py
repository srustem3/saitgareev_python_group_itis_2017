import doctest


def f(x):
    """
    >>> f(2)
    4
    """
    return x * x


if __name__ == '__main__':
    doctest.testmod()
