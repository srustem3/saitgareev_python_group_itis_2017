d1 = [['I', 'V', 'X'], ['X', 'L', 'C'], ['C', 'D', 'M']]

d2 = ['', '0', '00', '000', '01', '1', '10', '100', '1000', '02']

with open('Task2.txt', 'r', encoding='utf-8') as input_file:
    for line in input_file:
        my_list = line.split()
        for number in my_list:
            result = ''
            length = len(number) - 1
            for digit in number:
                pattern = d2[int(digit)]
                for symbol in pattern:
                    result += d1[length][int(symbol)]
                length -= 1
            print(result)
