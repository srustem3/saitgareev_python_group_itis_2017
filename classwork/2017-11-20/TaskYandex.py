import requests
from bs4 import BeautifulSoup

content = requests.get('https://yandex.ru').content
tree = BeautifulSoup(content, "lxml")
result = tree.find('ol').find_all('a')
for i in result:
    print(i.renderContents().decode('utf-8'))
