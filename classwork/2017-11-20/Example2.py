from bs4 import BeautifulSoup

txt = open("List.html", 'r').read()
tree = BeautifulSoup(txt, "lxml")
#  result = tree.find('div', {'class': 'movielist'}).get('class')
result = tree.find('ul').find_all('li')
print(list(map(lambda x: x.renderContents(), result)))
