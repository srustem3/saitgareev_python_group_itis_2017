from lxml import html

txt = open("List.html", 'r').read()

tree = html.fromstring(txt)
result = tree.xpath('//ul/li')
print(list(map(lambda x: x.text, result)))
