import datetime
import time

print(datetime.MINYEAR)
print(datetime.MAXYEAR)
print('=====================')

d = datetime.date(2017, 10, 9)
print(dir(d))
print(d.weekday())
print('======================')

d2 = datetime.date(2017, 6, 12)
td = d2 - d
print(dir(td))
print(td.days)
print(td.seconds)
print(td.total_seconds())
print('=======================')

td2 = datetime.timedelta(1, 1, 1)

d3 = datetime.date(year=2017, month=9, day=10)
dt = datetime.datetime.combine(d3, datetime.time(10, 10, 10))
print(dt)
print(datetime.datetime.now())
print(datetime.datetime.today())
print(time.ctime())
print('========================')

print(dt.strftime('%H:%M:%S %d/%m/%Y %A %a'))  # %y - последние 2 цифры года
dt2 = datetime.datetime.strptime('10 10 10', '%d %m %y')
print(dt2)
print('========================')
print(time.time())
print(time.sleep(2))  # В секундах
