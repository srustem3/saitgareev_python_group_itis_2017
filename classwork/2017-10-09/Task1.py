import re

import datetime

text = 'Some text here (2017-09-13 00:00] and there 2016-12-01 01:30. It is not time to sleep yet.'

delta = datetime.timedelta()
is_first = True
my_sum = 0

for i in re.finditer(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}', text):
    dt_line = i.group()
    dt = datetime.datetime.strptime(dt_line, '%Y-%m-%d %H:%M')
    print(dt)
    my_sum += dt.timestamp()

print(my_sum)
print('Sum is {}'.format(datetime.datetime.fromtimestamp(my_sum)))
