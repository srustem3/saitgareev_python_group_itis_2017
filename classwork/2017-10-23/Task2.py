import pygame

fire_list = list()

class Fire:
    def __init__(self, begin, end, screen, state):
        self.begin = begin
        self.end = end
        self.screen = screen
        self.state = state

    def render(self):
        pygame.draw.line(self.screen, (255, 255, 0), self.begin, self.end)
        if self.state == 'up':
            self.begin[1] -= 1
            self.end[1] -= 1
        if self.state == 'down':
            self.begin[1] += 1
            self.end[1] += 1
        if self.state == 'right':
            self.begin[0] += 1
            self.end[0] += 1
        if self.state == 'left':
            self.begin[0] -= 1
            self.end[0] -= 1


class SpaceShip:
    def __init__(self, screen):
        self.x = 400
        self.y = 300
        self.screen = screen
        self.state = 'right'

    def render(self):
        pygame.draw.circle(self.screen, (255, 255, 0), (self.x, self.y), 20, 0)
        gun_begin = (0, 0)
        gun_end = (0, 0)
        if self.state == 'right':
            gun_begin = (self.x + 20, self.y)
            gun_end = (self.x + 40, self.y)
        if self.state == 'left':
            gun_begin = (self.x - 20, self.y)
            gun_end = (self.x - 40, self.y)
        if self.state == 'up':
            gun_begin = (self.x, self.y - 20)
            gun_end = (self.x, self.y - 40)
        if self.state == 'down':
            gun_begin = (self.x, self.y + 20)
            gun_end = (self.x, self.y + 40)
        pygame.draw.line(screen, (255, 255, 0), gun_begin, gun_end)

    def move(self, option):
        if option == 'up':
            self.y -= 1
        if option == 'down':
            self.y += 1
        if option == 'right':
            self.x += 1
        if option == 'left':
            self.x -= 1
        self.state = option

    def fire(self):
        gun_begin = [0, 0]
        gun_end = [0, 0]
        if self.state == 'right':
            gun_begin = [self.x + 40, self.y]
            gun_end = [self.x + 60, self.y]
        if self.state == 'left':
            gun_begin = [self.x - 40, self.y]
            gun_end = [self.x - 60, self.y]
        if self.state == 'up':
            gun_begin = [self.x, self.y - 40]
            gun_end = [self.x, self.y - 60]
        if self.state == 'down':
            gun_begin = [self.x, self.y + 40]
            gun_end = [self.x, self.y + 60]
        fire_list.append(Fire(begin=gun_begin, end=gun_end, screen=screen, state=self.state))


pygame.display.init()
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption('My super spaceship game')
spaceship = SpaceShip(screen)
go = True
while go:
    screen.fill((0, 0, 0))
    screen.lock()

    spaceship.render()
    for fire in fire_list:
        fire.render()
    screen.unlock()
    pygame.display.flip()
    pygame.event.pump()
    pressed_list = pygame.key.get_pressed()
    if pressed_list[pygame.K_ESCAPE]:
        go = False
    if pressed_list[pygame.K_UP]:
        spaceship.move('up')
    if pressed_list[pygame.K_DOWN]:
        spaceship.move('down')
    if pressed_list[pygame.K_RIGHT]:
        spaceship.move('right')
    if pressed_list[pygame.K_LEFT]:
        spaceship.move('left')
    if pressed_list[pygame.K_SPACE]:
        spaceship.fire()
