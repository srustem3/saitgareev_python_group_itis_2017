import pygame
import time

#  Parameters
w = 800
h = 600
radius = 1
x = 400
y = 300

pygame.display.init()
screen = pygame.display.set_mode((w, h))
pygame.display.set_caption('Test')

# Line
# for i in range(100, 700):
#     screen.set_at((i, 300), (255, 0, 0))

# Not effective algorithm
# for i in range(0, w):
#     for j in range(0, h):
#         if (i - x)*(i-x) + (j-y)*(j-y) <= radius*radius:
#             screen.set_at((i, j), (255, 0, 0))

oj1 = 0
oj2 = 0

start_time = time.time()
fps = 0

r_min = 5
r_max = 300

radius = r_min + 1
delta = 1
change_time = time.time()

go = True

while go:
    screen.fill((0, 0, 0))
    screen.lock()
    # for i in range(x - radius, x + radius + 1):
    #     j1 = int(round(radius * radius - (i - x) * (i - x)) ** 0.5) + y
    #     j2 = -int(round(radius * radius - (i - x) * (i - x)) ** 0.5) + y
    #     if oj1 != 0 and oj2 != 0:
    #         pygame.draw.line(screen, (255, 0, 0), (i - 1, oj1), (i, j1))
    #         pygame.draw.line(screen, (255, 0, 0), (i - 1, oj2), (i, j2))
    #     oj1 = j1
    #     oj2 = j2
    #     screen.set_at((i, j1), (255, 0, 0))
    #     screen.set_at((i, j2), (255, 0, 0))
    pygame.draw.circle(screen, (255, 255, 0), (x, y), radius, 1)
    screen.unlock()
    pygame.display.flip()
    fps += 1
    if time.time() - start_time > 1:
        print(fps)
        start_time = time.time()
        fps = 0
    if time.time() - change_time > 0.01:
        radius += delta
        change_time = time.time()
    if radius == r_max:
        delta = -1
    elif radius == r_min:
        delta = 1
    pygame.event.pump()
    pressed_list = pygame.key.get_pressed()
    if pressed_list[pygame.K_ESCAPE]:
        go = False
    if pressed_list[pygame.K_UP]:
        y -= 1
    if pressed_list[pygame.K_DOWN]:
        y += 1
    if pressed_list[pygame.K_RIGHT]:
        x += 1
    if pressed_list[pygame.K_LEFT]:
        x -= 1
pygame.display.quit()
