import random

#  Parameters
desired_name = 'rustem'
generation_size = 100
selection_coefficient = 0.7  # Determines a part of the best names


def generate_names(number):
    lst = list()
    for word in range(number):
        new_word = ''
        for symbol in range(len(desired_name)):
            new_word += chr(ord('a') + random.randint(0, 25))
        lst.append(new_word)
    return lst


def fitness_function(tested):
    result = 0
    for (d, t) in zip(desired_name, tested):
        if d != t:
            result += 1
    return result


def crossing_over(lst1, lst2):
    res_list = list()
    for first_word, second_word in zip(lst1, lst2):
        new_word = ''
        for f, s in zip(first_word, second_word):
            if random.choice([True, False]):
                new_word += f
            else:
                new_word += s
        res_list.append(new_word)
    return res_list


#  We already expect sorted by fitness_function list
def create_generation(input_generation):
    res_lst = list()
    sample_size = int(len(input_generation) * selection_coefficient)
    # Crossing over among the best names
    res_lst += crossing_over(input_generation[0:sample_size], input_generation[1:sample_size])
    #  Mutations
    res_lst += crossing_over(input_generation[0:generation_size - sample_size], generate_names(generation_size
                                                                                               - sample_size))
    res_lst.sort(key=fitness_function)
    return res_lst


lst = generate_names(generation_size)
lst.sort(key=fitness_function)
print('Parent generation')
print('Fitness function: ' + str(fitness_function(lst[0])))
print(lst)
print('=======================================================')
i = 1
while lst[0] != desired_name:
    lst = create_generation(lst)
    print('Generation ' + str(i))
    print('Fitness function: ' + str(fitness_function(lst[0])))
    print(lst)
    print('=======================================================')
    i += 1
print(lst[0])
