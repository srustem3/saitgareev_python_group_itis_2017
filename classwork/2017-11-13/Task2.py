import multiprocessing

import requests


def f(url):
    response = requests.get(url)
    print(url)
    print(response.content)
    print('=============================')


if __name__ == '__main__':
    multiprocessing.freeze_support()
    p = multiprocessing.Pool()
    p.map(f, ['https://yandex.ru', 'https://google.com', 'https://mail.ru'])
