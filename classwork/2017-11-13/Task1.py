import random
import threading
from threading import Thread

import time


def generate(number, name, e1, e2):
    for i in range(number):
        e1.wait()
        print(name, random.randint(1, 100))
        e1.clear()
        e2.set()


start = time.time()
ee1 = threading.Event()
ee2 = threading.Event()

t1 = Thread(target=generate, args=(10, 't1', ee1, ee2))
t2 = Thread(target=generate, args=(10,'t2', ee1, ee2))
t1.start()
t2.start()
t1.join()
t2.join()
print(time.time() - start)
start = time.time()
generate(20, 'plain', ee1, ee1)
print(time.time() - start)
