import random
import multiprocessing
from multiprocessing import Process, freeze_support

import time


def generate(number, name):
    for i in range(number):
        print(name + str(random.randint(1, 100)))


start = time.time()
# ee1 = multiprocessing.Event()
# ee2 = multiprocessing.Event()

if __name__ == '__main__':
    freeze_support()
    t1 = Process(target=generate, args=(10, 't1 '))
    t2 = Process(target=generate, args=(10, 't2 '))
    t1.start()
    t2.start()
    t1.join()
    t2.join()
    print(time.time() - start)
    start = time.time()
    generate(20, 'plain')
    print(time.time() - start)
