import os

dirs = os.listdir('Example1')
result = 0
for i in dirs:
    if i[0] in 'aeiou':
        file = open('Example1/%s' % i, 'r')
        result += float(file.read())

print(result)
