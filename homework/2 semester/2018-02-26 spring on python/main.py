from container import container_manager
from test_classes import A, B

if __name__ == '__main__':
    container_manager.autowiring()
    b = container_manager.components['b']
    print(b.a.number)

#  https://stackoverflow.com/questions/306130/python-decorator-makes-function-forget-that-it-belongs-to-a-class
#  https://stackoverflow.com/questions/681953/python-class-decorator
