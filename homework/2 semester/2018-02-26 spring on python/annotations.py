import inspect

from container import container_manager


def component(class_to_save):
    container_manager.components[class_to_save.__name__.lower()] = class_to_save()
    return class_to_save


def autowired(setter):
    classname = inspect.getouterframes(inspect.currentframe())[1][3]
    container_manager.setters.append((classname, setter))
    return setter
