from annotations import component, autowired


@component
class A:
    def __init__(self):
        self.number = 5


@component
class B:
    @autowired
    def setA(self, a):
        self.a = a
