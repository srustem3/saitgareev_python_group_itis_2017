class DIContainer:
    def __init__(self):
        self.components = dict()
        self.setters = list()

    def autowiring(self):
        for cl_name, setter in self.setters:
            wired_class = self.components[setter.__name__.split('set')[1].lower()]
            wiring_class = self.components[cl_name.lower()]
            setter(wiring_class, wired_class)


container_manager = DIContainer()
