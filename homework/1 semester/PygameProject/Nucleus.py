import random

import pygame
from PIL import Image, ImageDraw
from pygame.rect import Rect

ART_CENTER = (30, 500)
ART_RADIUS = 25
GUN_RADIUS = 50
G = 10
V0 = 90

point_list = list()
house_list = list()
bomb = None


class House:
    def __init__(self, scr, left, height, width):
        self.left = left
        self.height = height
        self.width = width
        self.screen = scr

    def render(self):
        pygame.draw.rect(self.screen, (255, 0, 0), Rect(self.left, ART_CENTER[1], self.width, -self.height))


class Bomb:
    def __init__(self, scr, x, y):
        self.screen = scr
        self.x = int(x)
        self.y = int(y)
        self.begin_x = self.x
        self.begin_y = self.y
        print('fire - {} {}'.format(self.x, self.y))
        temp_x = x - ART_CENTER[0]
        temp_y = -y + ART_CENTER[1]
        self.tg = temp_y / temp_x
        self.cos = temp_x / ((temp_x ** 2 + temp_y ** 2) ** 0.5)

    def render(self):
        pygame.draw.circle(self.screen, (255, 255, 0), (int(self.x), int(self.y)), 5, 0)
        point_list.append((int(self.x), int(self.y)))
        if self.tg > 1:
            self.x += 1 / self.tg
        else:
            self.x += 1
        temp = self.x - self.begin_x
        self.y = self.begin_y - temp * self.tg + G * temp * temp / (2 * V0 * V0 * self.cos * self.cos)

    def collision_detection(self):
        if self.y >= ART_CENTER[1]:
            for house in house_list:
                if house.left - 5 <= self.x <= house.left + house.width + 5:
                    house_list.remove(house)
            global bomb
            bomb = None
            point_list.clear()


class Artillery:
    def calculate_gun_end_y(self):
        self.gun_end_y = -int((GUN_RADIUS ** 2 - (self.gun_end_x - ART_CENTER[0]) ** 2) ** 0.5) + ART_CENTER[1]

    def __init__(self, scr):
        self.screen = scr

        pil_size = ART_RADIUS * 2  # diameter
        pil_image = Image.new("RGBA", (pil_size, pil_size))
        pil_draw = ImageDraw.Draw(pil_image)
        pil_draw.pieslice((0, 0, pil_size - 1, pil_size - 1), 180, 0, fill=(255, 255, 255))

        mode = pil_image.mode
        size = pil_image.size
        data = pil_image.tobytes()

        self.image = pygame.image.fromstring(data, size, mode)
        self.image_rect = self.image.get_rect(center=ART_CENTER)

        self.gun_end_x = ART_CENTER[0] + GUN_RADIUS // 2
        self.calculate_gun_end_y()

    def render(self):
        self.screen.blit(self.image, self.image_rect)
        pygame.draw.line(self.screen, (255, 255, 255), ART_CENTER, (self.gun_end_x, self.gun_end_y))  # Gun

    def rotate(self, option):
        if option == 'down':
            if self.gun_end_x <= ART_CENTER[0] + GUN_RADIUS - 5:
                self.gun_end_x += 0.03
        if option == 'up':
            if self.gun_end_x >= ART_CENTER[0]:
                self.gun_end_x -= 0.03
        self.calculate_gun_end_y()

    def fire(self):
        global bomb
        if bomb is None:
            bomb = Bomb(screen, self.gun_end_x, self.gun_end_y)


if __name__ == '__main__':
    pygame.display.init()
    screen = pygame.display.set_mode((800, 600))
    pygame.display.set_caption('Nucleus')
    art = Artillery(screen)
    for i in range(10):
        house_list.append(House(screen, random.randint(100, 750), random.randint(10, 20), random.randint(10, 50)))
    go = True
    while go:
        screen.fill((0, 0, 0))
        screen.lock()
        pygame.draw.line(screen, (255, 255, 0), (0, ART_CENTER[1]), (800, ART_CENTER[1]))
        for point in point_list:
            screen.set_at(point, (255, 255, 0))
        for house in house_list:
            house.render()
        if bomb is not None:
            bomb.render()
            bomb.collision_detection()
        screen.unlock()
        art.render()
        pygame.display.flip()
        pygame.event.pump()
        pressed_list = pygame.key.get_pressed()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                go = False
        if pressed_list[pygame.K_ESCAPE]:
            go = False
        if pressed_list[pygame.K_UP]:
            art.rotate('up')
        if pressed_list[pygame.K_DOWN]:
            art.rotate('down')
        if pressed_list[pygame.K_SPACE]:
            art.fire()
