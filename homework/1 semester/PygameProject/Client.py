import socket
from threading import Thread, Event
import pygame
import Nucleus
from Nucleus import Artillery, house_list, House, ART_CENTER, point_list


is_new_event = False
can_go = False
coordinates = (0, 0)


class ClientThread:
    def __init__(self):
        self.host = 'localhost'
        self.port = 1234
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.connect((self.host, self.port))
        self.name = ''
        self.order = 0

    def set_name(self):
        self.name = input("Enter  ur name: ")

    def prepare_target(self):
        while True:
            if self.server.recv(10).decode('ascii') == "let's go":
                for i in range(10):
                    data = self.server.recv(9).decode('ascii')
                    data = data.split(" ")
                    left = int(data[0])
                    height = int(data[1])
                    width = int(data[2])
                    print(left, height, width)
                    house_list.append(House(Nucleus.screen, left, height, width))
                break

    def run(self):
        print(self.server.recv(20).decode('ascii'))
        self.set_name()
        self.server.send(self.name.encode('ascii'))
        self.order = int(self.server.recv(1).decode('ascii'))
        print(self.order)
        self.prepare_target()
        while True:
            current_player = int(self.server.recv(1).decode('ascii'))
            if self.order == current_player:
                global can_go
                can_go = True
                #  По иксу 2 символа, по y - 3 + разделитель = 6
                e1.wait()
                art_pos = '{}|{}'.format(int(Nucleus.art.gun_end_x), int(Nucleus.art.gun_end_y))
                self.server.send(str(art_pos).encode('ascii'))
                e1.clear()
            else:
                step = self.server.recv(6).decode('ascii').split('|')
                Nucleus.art.gun_end_x = int(step[0])
                Nucleus.art.gun_end_y = int(step[1])
                Nucleus.art.fire()
                print(str(current_player) + " - " + str(step))


def game():
    pygame.display.init()
    Nucleus.screen = pygame.display.set_mode((800, 600))
    pygame.display.set_caption('Nucleus')
    Nucleus.art = Artillery(Nucleus.screen)
    go = True
    while go:
        Nucleus.screen.fill((0, 0, 0))
        Nucleus.screen.lock()
        pygame.draw.line(Nucleus.screen, (255, 255, 0), (0, ART_CENTER[1]), (800, ART_CENTER[1]))
        for point in point_list:
            Nucleus.screen.set_at(point, (255, 255, 0))
        for house in house_list:
            house.render()
        if Nucleus.bomb is not None:
            Nucleus.bomb.render()
            Nucleus.bomb.collision_detection()
        Nucleus.screen.unlock()
        Nucleus.art.render()
        pygame.display.flip()
        pygame.event.pump()
        pressed_list = pygame.key.get_pressed()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                go = False
        if pressed_list[pygame.K_ESCAPE]:
            go = False
        if pressed_list[pygame.K_UP]:
            Nucleus.art.rotate('up')
        if pressed_list[pygame.K_DOWN]:
            Nucleus.art.rotate('down')
        global can_go
        if pressed_list[pygame.K_SPACE] and can_go:
            Nucleus.art.gun_end_x = int(Nucleus.art.gun_end_x)
            Nucleus.art.gun_end_y = int(Nucleus.art.gun_end_y)
            Nucleus.art.fire()
            e1.set()
            can_go = False


if __name__ == '__main__':
    client = ClientThread()
    e1 = Event()
    Thread(target=client.run).start()
    Thread(target=game).start()
