import socket
import random


class ServerThread:
    def __init__(self):
        self.host = ''
        self.port = 1234
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((self.host, self.port))
        self.socket.listen(3)
        self.connList = []
        self.names = []
        self.counter = 0

    def await(self):
        client, address = self.socket.accept()
        client.send("Welcome to Kimenia!".encode('ascii'))
        self.acquaintance(client)
        self.connList.append(client)

    def acquaintance(self, client):
        name = client.recv(20).decode('ascii')
        print(name + " is a new player")
        self.counter += 1
        client.send(str(self.counter).encode('ascii'))
        self.names.append(name)

    def create_target(self):
        for client in self.connList:
            client.send("let's go".encode('ascii'))
        for i in range(10):
            left = random.randint(100, 750)
            height = random.randint(10, 20)
            width = random.randint(10, 50)
            data = str(left) + " " + str(height) + " " + str(width)
            print(data)
            for client in self.connList:
                client.send(data.encode('ascii'))

    def go(self):
        for name in self.names:
            print(name)
        while self.names.__len__() == 3:
            order = 0
            while order < 3:
                for client in self.connList:
                    client.send(str(order + 1).encode('ascii'))
                answer = self.connList[order].recv(6).decode('ascii')
                for client in self.connList:
                    if client != self.connList[order]:
                        client.send(answer.encode('ascii'))
                order += 1
                if order == 3:
                    order = 0

    def run(self):
        while self.connList.__len__() < 3:
            self.await()
        self.create_target()
        self.go()


def main():
    server = ServerThread()
    server.run()


main()
