from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from django.urls import reverse

from comments.models import Message


def messages(request):
    if request.POST:
        new_message = Message()
        new_message.text = request.POST['text']
        new_message.save()
        return HttpResponseRedirect(reverse('messages'))
    context = {'messages': Message.objects.all()}
    return render(request, 'comments/messages.html', context)


def reply(request, comment_id):
    if request.POST:
        reply_to = get_object_or_404(Message, pk=comment_id)
        new_message = Message()
        new_message.text = request.POST['text']
        new_message.reply_to = reply_to
        new_message.save()
        return HttpResponseRedirect(reverse('messages'))
    context = {'reply_to': comment_id}
    return render(request, 'comments/reply.html', context)
