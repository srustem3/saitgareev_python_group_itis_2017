from django.db import models


class Message(models.Model):
    """ Messages for our chan """
    class Meta:
        verbose_name = u'Сообщение'
        verbose_name_plural = u'Сообщения'

    def __str__(self):
        return self.text

    text = models.TextField(verbose_name='Text')
    datetime = models.DateTimeField(verbose_name='Creating time', auto_now=True)
    reply_to = models.ForeignKey('Message', related_name='replies', null=True)
