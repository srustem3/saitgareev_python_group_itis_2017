from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'reply/(?P<comment_id>\d+)$', views.reply, name='reply'),
    url(r'^$', views.messages, name='messages'),
]
