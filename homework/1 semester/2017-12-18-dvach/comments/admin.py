from django.contrib import admin

from comments.models import Message

admin.site.register(Message)
