from bs4 import BeautifulSoup


def process(element):
    if element.name is not None:
        if element.name == 'table':
            table_render(element)
        elif element.name == 'ul':
            list_render(element)
        elif element.name == 'h1':
            print('h1: ' + final_render(element))
        elif element.name == 'h2':
            print('h2: ' + final_render(element))
        elif element.name == 'h3':
            print('h3: ' + final_render(element))
        elif element.name in ['div', 'html', 'title', 'body']:
            recursive_render(element)
        else:
            print(final_render(element))


def recursive_render(element):
    if element.name is not None:
        if element.children is not None:
            for c in element.children:
                process(c)


def final_render(element):
    return element.string


def list_render(element):
    for li in element.children:
        if li.name is not None:
            print('* ' + li.string)


def table_render(element):
    print('----------------------------')
    for tr in element.children:
        if tr.name is not None:
            for td in tr.children:
                if td.name is not None:
                    print(td.string + '|', end='')
            print()
            print('----------------------------')


content = open("SimplePage.html", 'r').read()
tree = BeautifulSoup(content, "lxml")
for child in tree.children:
    process(child)
