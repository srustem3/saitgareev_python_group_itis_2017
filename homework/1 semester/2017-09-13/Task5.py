import math


class RationalFraction:
    def __init__(self, x=0, y=1):  # If we set y to 0, we will have division to 0
        self.x = x
        self.y = y

    def __reduce__(self):
        gcd = math.gcd(self.x, self.y)
        return RationalFraction(self.x // gcd, self.y // gcd)

    def __add__(self, other):
        return RationalFraction(self.x * other.y + other.x * self.y, self.y * other.y).__reduce__()

    def __sub__(self, other):
        return RationalFraction(self.x * other.y - other.x * self.y, self.y * other.y).__reduce__()

    def __mul__(self, other):
        return RationalFraction(self.x * other.x, self.y * other.y).__reduce__()

    def __truediv__(self, other):
        return RationalFraction(self.x * other.y, self.y * other.x).__reduce__()

    def __str__(self):
        return "%s/%s" % (self.x, self.y)

    def value(self):
        return self.x / self.y

    def __eq__(self, other):
        reduced_self = self.__reduce__()
        reduced_other = other.__reduce__()
        return reduced_self.x == reduced_other.x and reduced_self.y == reduced_other.y

    def number_part(self):
        return self.x // self.y


if __name__ == '__main__':
    print((RationalFraction(1, 2) + RationalFraction(2, 4)).value() == 1.0)
    print((RationalFraction(1, 2) / RationalFraction(2, 4)).number_part() == 1)
    print(RationalFraction(3, 2) - RationalFraction(1, 2) == RationalFraction(10, 10))
