dictionary = {}
with open('Task7.txt') as file:
    for line in file:
        splitted_line = line.split()
        for word in splitted_line:
            word_lower = word.lower()
            dictionary.setdefault(word_lower, 0)
            dictionary[word_lower] += 1
print(dictionary)
