rome_symbols = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}


def rome_to_arab_convert(number):
    result = 0
    i = 0
    while i < len(number) - 1:
        difference = rome_symbols[number[i]] - rome_symbols[number[i + 1]]
        if difference >= 0:
            result += rome_symbols[number[i]]
        else:
            result -= rome_symbols[number[i]]
        i += 1
    result += rome_symbols[number[len(number) - 1]]
    return result


if __name__ == '__main__':
    print('Enter number')
    print(rome_to_arab_convert(input()))
