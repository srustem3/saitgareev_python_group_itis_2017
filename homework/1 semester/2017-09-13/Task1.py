rome_symbol_groups = [['I', 'V', 'X'], ['X', 'L', 'C'], ['C', 'D', 'M']]

common_pattern = ['', '0', '00', '000', '01', '1', '10', '100', '1000', '02']


def arab_to_rome_convert(number):
    result = ''
    length = len(number) - 1
    for digit in number:
        pattern = common_pattern[int(digit)]
        for symbol in pattern:
            result += rome_symbol_groups[length][int(symbol)]
        length -= 1
    return result


if __name__ == '__main__':
    print('Enter number')
    print(arab_to_rome_convert(input()))
