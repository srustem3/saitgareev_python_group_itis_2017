import math


class Vector2D:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Vector2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector2D(self.x - other.x, self.y - other.y)

    def mul_to_number(self, number):
        return Vector2D(self.x * number, self.y * number)

    def __str__(self):
        return 'x=%s;y=%s' % (self.x, self.y)

    def len(self):
        return math.sqrt(self.x * self.x + self.y * self.y)

    def __mul__(self, other):
        return self.x * other.x + self.y * other.y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y


if __name__ == '__main__':
    print((Vector2D(1, 2) + Vector2D(2, 2)).len())
    print(Vector2D() - Vector2D(2, 2).mul_to_number(3))
    print(Vector2D(2, 0) * Vector2D(0, 1) == 0)
