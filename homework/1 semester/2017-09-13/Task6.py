print('Enter text here')
input_text = input().lower()
dictionary = dict((chr(x), 0) for x in range(ord('a'), ord('z') + 1))
for i in input_text:
    if i in dictionary:
        dictionary[i] += 1
print(dictionary)
